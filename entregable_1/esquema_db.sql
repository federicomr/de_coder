create table sube_transacciones_dia (
	fecha date,
	nombre_empresa varchar,
	linea varchar,
	es_amba varchar,
	tipo_transporte varchar,
	jurisccion varchar,
	provincia varchar,
	municipio varchar,
	cantidad int,
	dato_preliminar varchar
) diststyle auto;

create table sube_usuarios_dia (
	fecha date,
	es_amba varchar,
	motivo_tarifa_social varchar,
	genero varchar,
	tipo_transporte varchar,
	cantidad_tarjetas int,
	dato_preliminar varchar
) diststyle auto;