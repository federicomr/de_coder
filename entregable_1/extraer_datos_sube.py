import os
import json
import pandas as pd

# Constantes
URL_BASE = "https://archivos-datos.transporte.gob.ar/upload"

CARPETA_DATOS = "data"
CARPETA_DATOS_SUBE = "sube"
RUTA_DATOS_SUBE = os.path.join(CARPETA_DATOS, CARPETA_DATOS_SUBE)

ARCHIVO_PARAMETROS_SUBE = "parametros_datasets.json"
RUTA_ARCHIVOS_PARAMETROS = os.path.join(CARPETA_DATOS, ARCHIVO_PARAMETROS_SUBE)


def obtener_parametros_datasets(ruta_archivo_parametros:str=RUTA_ARCHIVOS_PARAMETROS) -> dict:
    """Levanta el archivo json con los parametros de los dataset a procesar.

    Args:
        ruta_archivo_parametros (str, optional): Carpeta donde esta el archivo. 
        Defaults to RUTA_ARCHIVOS_PARAMETROS.

    Returns:
        dict: Devuelve un diccionario con los parametros de cada dataset.
    """

    with open(ruta_archivo_parametros, "r") as archivo_parametros:
        parametros_datasets = json.load(archivo_parametros)

    return parametros_datasets


def descargar_csv(dataset:str, nombre_archivo:str, url_base:str=URL_BASE) -> pd.DataFrame:
    """Descarga el csv de la ul indicada.

    Args:
        url_base (str): La url de datos.gob.ar
        dataset (str): nombre del dataset en la url de descarga
        nombre_archivo (str): Nombre del archivo csv en la url de descarga

    Returns:
        pd.DataFrame: El csv resultante en un DataFrame
    """

    url_archivo = "/".join([url_base, dataset, nombre_archivo])

    try:
        respuesta = pd.read_csv(url_archivo)
    except Exception as e:
        print("Ocurrió un error al intentar obtener los datos:")
        print(e)
        respuesta = None


    return respuesta


def guardar_csv(tabla:pd.DataFrame, nombre_archivo:str, carpeta_destino:str=RUTA_DATOS_SUBE) -> None:
    """Guarda un DataFrame en un csv en una carpeta local.

    Args:
        tabla (pd.DataFrame): tabla que se va a guardar.
        carpeta_destino (str): Carpeta donde se va a guardar el csv.
        nombre_archivo (str): Nombre del archivo que se va a guardar.
    """
    tabla.to_csv(os.path.join(carpeta_destino, nombre_archivo), index=False)


def descargar_datos_historicos(datasets:dict) -> None:
    """Descarga todos los archivos csv de los datasets indicados y los guarda
      en una carpeta local.

    Args:
        datasets (dict): Diccionario con los datasets y archivos a descargar.
    """
    for nombre_dataset, parametros in datasets.items():
        for _, nombre_archivo_csv in parametros["archivos_csv"].items():
            
            # Descargar archivo desde la web de datos abiertos
            datos_anio = descargar_csv(parametros["codigo_dataset"], nombre_archivo_csv)
            nombre_archivo_guardar = nombre_dataset + "_" + nombre_archivo_csv
            
            # Guardar archivo en un csv local
            guardar_csv(datos_anio, nombre_archivo_guardar)


def descargar_archivos_anio(datasets:dict, anio:int) -> None:
    """Descarga los archivos del año y de los datasets indicados y los guarda
      en una carpeta local.

    Args:
        datasets (dict): Diccionario con los datasets que se quiere obtener.
        anio (int): Año de los datos que se busca descargar
    """
    for nombre_dataset, parametros in datasets.items():
        nombre_archivo_csv_anio = parametros["archivos_csv"][str(anio)]

        # Descargar archivo desde la web de datos abiertos
        df_anio = descargar_csv(parametros["codigo_dataset"], nombre_archivo_csv_anio)
        nombre_archivo_guardar = nombre_dataset + "_" + nombre_archivo_csv_anio
        
        # Guardar archivo en un csv local
        guardar_csv(df_anio, nombre_archivo_guardar)
        
    
if __name__ == "__main__":
    # Descargar datos del año actual
    parametros = obtener_parametros_datasets()
    descargar_archivos_anio(parametros, 2024)