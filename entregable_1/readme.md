## Entregable 1

## Datos publicos SUBE
Para el trabajo se seleccionó como fuente los datos abiertos de la tarjeta SUBE que son publicados en el portal oficial del gobierno nacional (https://datos.transporte.gob.ar).\
Se extrajeron dos datasets. Primero la cantidad de viajes (transacciones) realizados por dia, por tipo de transporte y linea. Segundo, la cantidad de usuarios (tarjetas únicas) que viajaron por dia, tipo de transporte y tipo de tarifa.

## Instalación
Para ejecutar la extracción en primer lugar se debe instalar la libreria `pandas` con el comando

    $ pip install pandas

Luego, asegurarse que exista la carpeta _data_ dentro de la carpeta del proyecto y la carpeta _sube_ dentro de la primera. O cambiar las constantes `CARPETA_DATOS` y `CARPETA_DATOS_SUBE` dentro del `archivo extraer_datos_sube.py` a los nombres de las carpetas donde se guardaran los csv descargados.

## Ejecución
El proceso de extracción por defecto descarga los datos de los datasets del 2024. Si el archivo ya existe lo sobreescribe.

    $ python extraer_datos_sube.py

Para ejecutar el proceso para todos los años disponibles es necesario modificar el .py de la siguiente forma:

```python
# Version actual
#if __name__ == "__main__":
#    # Descargar datos del año actual
#    parametros = obtener_parametros_datasets()
#    descargar_archivos_anio(parametros, 2024)

# Version proceso completo
if __name__ == "__main__":
    parametros = obtener_parametros_datasets()
    descargar_datos_historicos(parametros)
```

## Modelo base de datos
El archivo `esquema_db.sql` contiene las sentencias sql para crear las dos tablas donde se guardarán los datos de cada uno de los datasets.