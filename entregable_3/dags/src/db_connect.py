import os
from dotenv import load_dotenv
import redshift_connector
from sqlalchemy import create_engine
from sqlalchemy.engine.url import URL
import pandas as pd

import src.constants as C

class Connection:
    """
    Conexion a base de datos
    
    Parametros
    ----------
    - dialect(str): motor de base de datos a utilizar. Por defecto redshift.
    - db_name(str): nombre de la base de datos a la cual se quiere conectar. 
      Debe coinicidir con el utilizado en el archivo .env.
    """
    def __init__(self, dialect='redshift+redshift_connector', db_name=None):
        self.db = db_name
        self.dialect = dialect

    def load_env(self,file_path:str=C.ENV_FILE):
        try:
            load_dotenv(file_path)
        except Exception as e:
            print(e)
        return 'Variables importadas correctamente.'

    def create_con(self):
        """
        Creates a connection to the Redshift database
        """
        db = self.db.upper()
        
        url_db = URL.create(
            drivername = "redshift+psycopg2",
            host = os.environ.get(f"{db}_HOST"),
            database = os.environ.get(f"{db}_DB"),
            port = os.environ.get(f"{db}_PORT"),
            username = os.environ.get(f"{db}_USER"),
            password = os.environ.get(f"{db}_PASSWORD")
        )

        redshift_con = create_engine(url_db)

        try:
            redshift_con.connect()
        except Exception as e:
            return e
        return redshift_con
    

    def execute_select(self, query:str) -> pd.DataFrame:
        """Execute a select statement against the Redshift Database.

        Args:
            query (str): The SELECT query.

        Returns:
            pd.DataFrame: A table with the desired output.
        """

        with self.create_con() as redshift_con:
            with redshift_con.cursor() as cursor:
                r = cursor.execute(query)
                df = pd.DataFrame(r)
        
        return df
    

        
