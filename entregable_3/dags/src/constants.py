import os

URL_BASE = "https://archivos-datos.transporte.gob.ar/upload"

AIRFLOW_FOLDER = os.getcwd()
PROJECT_FOLDER = "/opt/airflow/dags/src/"

ENV_FILE = os.path.join(AIRFLOW_FOLDER, ".env")

CARPETA_DATOS = "data"
RUTA_DATOS_SUBE = os.path.join(AIRFLOW_FOLDER, CARPETA_DATOS)

#ARCHIVO_PARAMETROS_SUBE = "datasets_parameters.json"
ARCHIVO_PARAMETROS_SUBE = os.path.join(PROJECT_FOLDER, "datasets_parameters.json")