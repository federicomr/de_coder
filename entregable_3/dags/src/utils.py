import os
import json
import pandas as pd

import src.constants as C


def get_datasets_parameters(
        ruta_archivo_parametros:str=C.ARCHIVO_PARAMETROS_SUBE
        ) -> dict:
    """Levanta el archivo json con los parametros de los dataset a procesar.

    Args:
        ruta_archivo_parametros (str, optional): Carpeta donde esta el archivo. 
        Defaults to RUTA_ARCHIVOS_PARAMETROS.

    Returns:
        dict: Devuelve un diccionario con los parametros de cada dataset.
    """

    with open(ruta_archivo_parametros, "r") as archivo_parametros:
        parametros_datasets = json.load(archivo_parametros)

    return parametros_datasets


def get_data_csv(file_name:str, 
                 source_folder:str=C.RUTA_DATOS_SUBE) -> pd.DataFrame:
    """Get the data of the indicated csv to a DataFrame.

    Args:
        file_name (str): Name of the csv file to get.
        source_folder (str): Route of the folder with the csv

    Returns:
        pd.DataFrame: A table with the csv data
    """
    return pd.read_csv(os.path.join(source_folder, file_name))


def load_csv(tabla:pd.DataFrame, nombre_archivo:str, 
                carpeta_destino:str=C.RUTA_DATOS_SUBE) -> None:
    """Load the DataFrame into a csv file in a local folder.

    Args:
        tabla (pd.DataFrame): tabla que se va a guardar.
        carpeta_destino (str): Carpeta donde se va a guardar el csv.
        nombre_archivo (str): Nombre del archivo que se va a guardar.
    """
    tabla.to_csv(os.path.join(carpeta_destino, nombre_archivo), index=False)