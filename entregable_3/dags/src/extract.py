import pandas as pd
from typing import Union

import src.utils as utils
import src.constants as C


def download_csv(dataset:str, file_name:str, 
                  url_base:str=C.URL_BASE) -> Union[pd.DataFrame, None]:
    """Download the csv from the given url.

    Args:
        url_base (str): The url of open data portal.
        dataset (str): Name of the dataset to download.
        nombre_archivo (str): Name of the file to download.

    Returns:
        pd.DataFrame: The data extracted from the csv downloaded.
    """

    url_file = "/".join([url_base, dataset, file_name])

    try:
        csv_file = pd.read_csv(url_file)
    except Exception as e:
        print("An error ocurred trying to get the data.")
        print(e)
        csv_file = None


    return csv_file


def download_historic_data(dataset_name:str, dataset_parameters:dict) -> None:
    """Download all the csv files of given dataset and save them in a local
      folder.

    Args:
        dataset_name (str): Name of the dataset to download.
        datasets_parameters (dict): Parameters of the dataset and the files 
        names to download.
    """
    for _, csv_file_name in dataset_parameters["csv_files"].items():
        
        # Descargar archivo desde la web de datos abiertos
        year_data = download_csv(dataset_parameters["dataset_code"], 
                                    csv_file_name)
        file_name_to_load = dataset_name + "_" + csv_file_name
        
        # Guardar archivo en un csv local
        utils.load_csv(year_data, file_name_to_load)


def download_year_file(dataset_name:str, 
                       dataset_parameters:dict, 
                       year:str) -> None:
    """Download the file from given dataset and year and saves it in a local
     folder.

    Args:
        dataset_name (str): Name of the dataset to download.
        datasets_parameters (dict): Parameters of each dataset and the file
        name to download.
        anio (int): Year of the dataset file to download
    """
    year_csv_file_name = dataset_parameters["csv_files"][year]

    # Descargar archivo desde la web de datos abiertos
    df_year = download_csv(dataset_parameters["dataset_code"], 
                            year_csv_file_name)
    file_name_to_load = (
        dataset_name + "_" + year_csv_file_name
    )
    
    # Guardar archivo en un csv local
    utils.load_csv(df_year, file_name_to_load)
        
