import pandas as pd
from src.db_connect import Connection


def load_redshift(df_dataset:pd.DataFrame, table_name:str) -> None:
    """Load the given dataset to the Redshift database"""

    c = Connection(db_name="redshift")
    c.load_env()
    redshift_con = c.create_con()

    df_dataset.to_sql(
        table_name, 
        con=redshift_con, 
        if_exists="append", 
        index=False,
        method="multi"
        )
    

def delete_records_redshift(table_name:str, year:str) -> None:
    """Delete all rows of the selected table of the indicated year.

    Args:
        table_name (str): Name of the table.
        year (str): year of the rows to delete.
    """

    c = Connection(db_name="redshift")
    c.load_env()
    redshift_con = c.create_con()

    query_delete = f"""
        DELETE FROM {table_name} WHERE date_part('year', fecha) = {year} 
    """

    with redshift_con.connect() as con:
        con.execute(query_delete)

