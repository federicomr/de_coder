from datetime import datetime, timedelta

# The DAG object; we'll need this to instantiate a DAG
from airflow.models.dag import DAG

# Operators; we need this to operate!
from airflow.operators.python import PythonOperator
from airflow.operators.bash import BashOperator

# Import project functions
from src.etl import run_year_proccess

with DAG(
    "Update_SUBE_data",
    # These args will get passed on to each operator
    # You can override them on a per-task basis during operator initialization
    default_args={
        "start_date":datetime(2024, 4, 5),
        "depends_on_past": False,
        "email": ["federico.m.rapp@gmail.com"],
        "email_on_failure": False,
        "email_on_retry": False,
        "retries": 1
    },
    description="Update the transactions and unique cards  SUBE's data to the database",
    schedule="0 20 * * *",
    catchup=False,
    tags=["sube","transactions","unique_cards"],
) as dag:

    # t1, t2 and t3 are examples of tasks created by instantiating operators
    t1 = PythonOperator(
        task_id="update_transactions",
        python_callable=run_year_proccess,
        op_kwargs={"dataset_name": "transactions"}
    )

    t2 = PythonOperator(
        task_id="update_unique_cards",
        python_callable=run_year_proccess,
        op_kwargs={"dataset_name": "unique_cards"}
    )
    
    
    # t1 = BashOperator(
    #     task_id="update_transactions",
    #     bash_command="python etl_command.py update_dataset_year 2024 transactions",
    # )

    # t2 = BashOperator(
    #     task_id="update_unique_cards",
    #     bash_command="python etl_command.py update_dataset_year 2024 unique_cards",
    # )

    t1 >> t2