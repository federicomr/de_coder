#!/bin/bash

# Build the image
docker build -t airflow-image .

# Run container
docker run -d --name airflow-sube -p 8080:8080 airflow-image

# Create the admin user
docker exec -it airflow-sube airflow users delete -u admin 
docker exec -it airflow-sube airflow users create --role Admin --username admin --email admin --firstname admin --lastname admin --password admin