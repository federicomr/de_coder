## Entregable 3
El proyecto permite ejecutar la actualización diaria de los datos de SUBE que son volcados a la instancia de Amazon Redshift. 

## Datos publicos SUBE
Para el trabajo se seleccionó como fuente los datos abiertos de la tarjeta SUBE que son publicados en el portal oficial del gobierno nacional (https://datos.transporte.gob.ar).\
Se trabajó con dos datasets. Primero la cantidad de viajes (transacciones/transactions) realizados por dia, por tipo de transporte y linea. Segundo, la cantidad de usuarios (tarjetas únicas/unique cards) que viajaron por dia, tipo de transporte y tipo de tarifa.

## Prerequisitos
- Tener instalado Docker

## Instalación
El proceso se ejecuta enteramente dentro de un contenedor de Docker. Para levantar Airflow con el DAG que ejecuta el proceso es necesario seguir los siguientes pasos. 

Primero, Debe contar con un archivo `.env` con las credenciales de acceso a la base de datos Redshift.̣ Puede copiar el modelo y completar con los datos correspondientes.

    $ cp env-template .env

Segundo, generar la imagen a partir del `Dockerfile`. Para ello, ejecutar desde la carpeta del proyecto.

    $ docker build -t airflow-image .

Luego, generar un contenedor donde se correrá Airflow y el proceso de actualizacion de datos.

    $ docker run -d --name airflow-sube -p 8080:8080 airflow-image

Finalmente, generar el usuario `admin` de la interfaz web de Airflow para poder acceder desde allí.

    $ docker exec -it airflow-sube airflow users create --role Admin --username admin --email admin --firstname admin --lastname admin --password admin

En caso de que el comando anterior informe que el usuario admin ya existe deberá borrarlo con el comando que aparece más abajo y volver a ejecutar la creación.

    $ docker exec -it airflow-sube airflow users delete -u admin 

**Alternativa**: Si se encuentra en un entorno Linux puede ejecutar todos los pasos anteriores simplemente corriendo el script `install.sh`. Lo único que no incluye es la carga de las credenciales en el archivo `.env`. Por lo que en ese caso deberá realizar ese paso de forma manual.

## Ejecución
Para ejecutar el proceso debe acceder a la interfaz web de Airflow (localhost:8080) y ejecutar el DAG `Update_SUBE_data`.
