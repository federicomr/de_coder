import os

URL_BASE = "https://archivos-datos.transporte.gob.ar/upload"

AIRFLOW_FOLDER = os.getcwd()
PROJECT_FOLDER = "/opt/airflow/plugins/sube/"

ENV_FILE = os.path.join(PROJECT_FOLDER, ".env")

FILES_FOLDER = "files"
FILES_FOLDER_PATH = os.path.join(PROJECT_FOLDER, FILES_FOLDER)

# ---------------------------------------------------------------------------
# SUBE data

DATASETS_PARAMETERS_PATH = os.path.join(PROJECT_FOLDER, "datasets_parameters.json")

# Intermediate files
TRANSFORMED_TRANSACTIONS_FILE = "transformed_transactions.csv"
TRANSFORMED_UNIQUE_CARDS_FILE = "transformed_unique_cards.csv"
TRANSFORMED_UNIQUE_CARDS_TOTAL_FILE = "transformed_unique_cards_daily_total.csv"

# ---------------------------------------------------------------------------
# Normalize geo data DAG

# Intermediate files
UNIQUE_VALUES = "{}_unique_values.txt"
NORMALIZED_VALUES = "normalized_{}.csv"

# Database destination tables
DESTINATION_TABLE = "normalizador_{}"

# Geographic levels keywords
GEO_LEVELS = {
    "province": {
        "sube_table":"provincia",
        "georref_entity_name":"provincias",
        "destination_table_suffix": "provincias"
    },
    "city": {
        "sube_table":"municipio",
        "georref_entity_name":"municipios",
        "destination_table_suffix": "municipios"
    }
}

# ---------------------------------------------------------------------------
# Emails default parameters

EMAIL_ADDRESS_FROM = ""
EMAIL_ADDRESS_TO = ""

