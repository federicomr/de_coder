import os
import json
import pandas as pd

import sube.constants as C


def get_datasets_parameters(
        parameters_file_path:str=C.DATASETS_PARAMETERS_PATH
        ) -> dict:
    """
    Read and get datasets parameters from the parameters json file.

    Args:
        parameters_file_path (str, optional): Route to the parameters file.
        Defaults to constant DATASETS_PARAMETERS_PATH.

    Returns:
        dict: Devuelve un diccionario con los parametros de cada dataset.
    """

    with open(parameters_file_path, "r") as file_parameters:
        datasets_parameters = json.load(file_parameters)

    return datasets_parameters


def get_data_csv(file_name:str, 
                 source_folder:str=C.FILES_FOLDER_PATH) -> pd.DataFrame:
    """Get the data of the indicated csv to a DataFrame.

    Args:
        file_name (str): Name of the csv file to get.
        source_folder (str): Route of the folder with the csv

    Returns:
        pd.DataFrame: A table with the csv data
    """
    return pd.read_csv(os.path.join(source_folder, file_name))


def load_csv(table:pd.DataFrame, file_name:str, 
                folder_path:str=C.FILES_FOLDER_PATH) -> None:
    """Load the DataFrame into a csv file in a local folder.

    Args:
        table (pd.DataFrame): Table to save.
        folder_path (str): Folder where the table will be saved.
        file_name (str): Name of the csv file to save.
    """
    table.to_csv(os.path.join(folder_path, file_name), index=False)


def get_txt_data(filename:str, source_folder:str=C.FILES_FOLDER_PATH):
    """Get the data of the indicated txt file.

    Args:
        file_name (str): Name of the csv file to get.
        source_folder (str): Route of the folder with the txt file.

    Returns:
        list: Where each line is an element of the list.
    """
    with open(os.path.join(source_folder, filename), "r") as file_data:
        unique_values = [linea.replace('\n','') 
                         for linea in file_data.readlines()]
    
    return unique_values


def load_txt(data:list, filename:str, folder_path:str=C.FILES_FOLDER_PATH) -> None:
    """Load the given list to a txt local file.

    Args:
        data (list): The data to load in a list object.
        filename (str): Name of the file to save.
        folder_path (str): Path of the parent folder.
    """
    with open(os.path.join(folder_path, filename), "w") as destination_file:
        for value in data:
            destination_file.write(value + "\n")