import os
import smtplib
import logging
from dotenv import load_dotenv
from email.message import EmailMessage

import sube.constants as C

logger = logging.getLogger(__name__)


def send_email(
    subject:str,  
    body:str,
    email_from:str=C.EMAIL_ADDRESS_FROM, 
    email_to:str=C.EMAIL_ADDRESS_TO
    ) -> None:
  """Send an email using the account defined in the env file

  Args:
      subject (str): _description_
      email_from (str): _description_
      email_to (str): _description_
      body (str): _description_
  """

  if email_from == "" or email_to == "":
    logger.warning("'email_from' or 'email_to' parameters are empty. Did you forget to fill in these variables in constants.py?")
    return 

  # Get email credentials 
  load_dotenv(C.ENV_FILE)
  smtp_server = os.environ.get("EMAIL_SMTP_SERVER")
  smtp_port = os.environ.get("EMAIL_SMTP_PORT")
  smtp_address = os.environ.get("EMAIL_SMTP_ADDRESS")
  smtp_key = os.environ.get("EMAIL_SMTP_KEY")


  # Create a SMTP connection with server 
  with smtplib.SMTP(smtp_server, smtp_port) as server:

    # Create email message
    msg = EmailMessage()
    msg["Subject"] =subject
    msg["From"] = email_from
    msg["To"] = email_to

    msg.set_content(body)

    # Login and send the message
    server.login(smtp_address, smtp_key)
    server.send_message(msg)




