import pandas as pd

from sube.db_connect import Connection
import sube.utils as utils
import sube.constants as C
from sube.email_manager import send_email


def load_redshift(df:pd.DataFrame, table_name:str) -> None:
    """Load the given dataset to the Redshift database"""

    c = Connection(db_name="redshift")
    c.load_env()
    redshift_con = c.create_con()

    df.to_sql(
        table_name, 
        con=redshift_con, 
        if_exists="append", 
        index=False,
        method="multi",
        chunksize=10000
        )
    

def delete_records_redshift(table_name:str, year:str) -> None:
    """Delete all rows of the selected table of the indicated year.

    Args:
        table_name (str): Name of the table.
        year (str): year of the rows to delete.
    """

    c = Connection(db_name="redshift")
    c.load_env()
    redshift_con = c.create_con()

    query_delete = f"""
        DELETE FROM {table_name} WHERE date_part('year', fecha) = {year} 
    """

    with redshift_con.connect() as con:
        con.execute(query_delete)


def upsert_current_year_redshift(dataset_name:str):
    """Delete all records with the same year than the dataset to upload

    Args:
        dataset_name (str): Name of the dataset to upsert.
    """

    # Get the table name from the parameters json
    dataset_parameters = utils.get_datasets_parameters()[dataset_name]
    
    if dataset_name == "unique_cards":

        # Get the data to upload
        df_unique_cards = (
            utils.get_data_csv(C.TRANSFORMED_UNIQUE_CARDS_FILE)
        )
        df_daily_totals = (
            utils.get_data_csv(C.TRANSFORMED_UNIQUE_CARDS_TOTAL_FILE)
        )

        # Get destination tables names
        unique_cards_table = (
            dataset_parameters["destination_table"]["unique_cards"]
        )
        daily_totals_table = (
            dataset_parameters["destination_table"]["unique_cards_daily_totals"]
        )

        year = pd.to_datetime(df_unique_cards["fecha"]).max().year

         # First delete all records from current year
        delete_records_redshift(unique_cards_table, year)

         # Second, insert new values from current year
        load_redshift( df_unique_cards, unique_cards_table)
        
        # First delete all records from current year
        delete_records_redshift(daily_totals_table, year)
        
        # Second, insert new values from current year
        load_redshift(df_daily_totals, daily_totals_table)
        
        # Send an email about the success upload
        subject = "Update of unique_cards tables has been completed"
        email_body = """
        The update proccess of {0} and {1} tables has been completed successfully. New data is available in Redshift Data Warehouse.\n
        Updated year: {2}\n
        Total {0} records uploaded: {3}\n
        Total {1} records uploaded: {4}\n
        \n
        This is an automatic email sent by Airflow. Please, do not reply it.
        """
        send_email(
            subject,
            email_body.format(
                unique_cards_table,
                daily_totals_table,
                year,
                df_unique_cards.shape[0],
                df_daily_totals.shape[0]                
            )
        )


    elif dataset_name == "transactions":

        table_name = dataset_parameters["destination_table"]

        # Get the data to upload
        df_dataset = utils.get_data_csv(C.TRANSFORMED_TRANSACTIONS_FILE)
        year = pd.to_datetime(df_dataset["fecha"]).max().year

        # First delete all records from current year
        delete_records_redshift(table_name, str(year))

        # Second, insert new values from current year
        load_redshift(df_dataset, table_name)

        # Send an email about the success upload
        subject = "Update of transactions tables has been completed"
        email_body = """
        The update proccess of {0} table has been completed successfully. New data is available in Redshift Data Warehouse.\n
        Updated year: {1}\n
        Total {0} records uploaded: {2}\n
        \n
        This is an automatic email sent by Airflow. Please, do not reply it.
        """
        send_email(
            subject,
            email_body.format(
                table_name,
                year,
                df_dataset.shape[0]           
            )
        )