import os
import pandas as pd
import requests

import sube.utils as utils
import sube.constants as C
from sube.db_connect import Connection


def get_unique_values(level:str="province") -> None:
    """Get the unique values of the desired administrative level. Export the 
    result set to a local intermediate file.

    Args:
        level (str, optional): The administrative level to get the values of.
        Defaults to "province".
    """  
    
    con = Connection(db_name="redshift")

    query = """
    SELECT DISTINCT {0} as values 
    FROM sube_transacciones_dia
    WHERE {0} is NOT NULL
    """

    level_table = C.GEO_LEVELS[level]["sube_table"]

    unique_values = con.execute_select(query.format(level_table))
    unique_values_list = unique_values["values"].to_list()

    # Save the result list in a local txt file.
    utils.load_txt(unique_values_list, C.UNIQUE_VALUES.format(level))



def normalize_all_values(level:str="province") -> None:
    """Normalize all the values from the unique values of the given list.

    Args:
        unique_values (list): The unique values to normalize.

    Returns:
        pd.DataFrame: A table with the equivalencies between the current 
        values and the normalized ones.
    """

    def request_geoapi(geo_entity_value:str, level:str) -> dict:
        """Request the Georref API to get the normalize value of the searched 
        geo entity value.
        https://www.argentina.gob.ar/datos-abiertos/georef/openapi

        Args:
            geo_entity_value (str): The name of the entity to normalize.
            level (str, optional): The administrative level to get the values of.
            Defaults to "province".

        Returns:
            dict: A dict with the searched entity as key and the entity's 
            normalized name as value.
        """
        
        level_entity_name = C.GEO_LEVELS[level]["georref_entity_name"]

        endpoint = f"https://apis.datos.gob.ar/georef/api/{level_entity_name}"
        payload = {
            "nombre":geo_entity_value, 
            "aplanar": True, 
            "campos":"basico", 
            "max":1
            }

        response = requests.get(endpoint, params=payload)

        if response.status_code != 200:
            print(response.status_code)
            print(response.text)
            return

        return response.json()
    

    def get_normalized_value(response_data:dict, level:str) -> dict:
        """Build a dictionary with the to normalize value and the normalized one
        from the API Georref response data.

        Args:
            response_data (dict): The data obtained from the API Georref.

        Returns:
            dict: A dict with the searched entity as key and the entity's 
            normalized name as value.
        """
        level_entity_name = C.GEO_LEVELS[level]["georref_entity_name"]
        current_value = response_data["parametros"]["nombre"]

        try:
            normalized_value = response_data[level_entity_name][0]["nombre"]
        except:
            normalized_value = None

        return {
            "current_value": current_value,
            "normalized_value": normalized_value
        }

    normalized_values = {}

    # Get the level unique values from local file
    unique_values = utils.get_txt_data(C.UNIQUE_VALUES.format(level))

    # Iterate over each unique value
    for index, unique_value in enumerate(unique_values):

        # Get the normalized data from the API Georref endpoint
        normalized_data = request_geoapi(unique_value, level)

        # Extract the normalized entity value
        value = get_normalized_value(normalized_data, level)

        # Load the value in all values dict
        normalized_values[index] = value

    result = pd.DataFrame.from_dict(normalized_values, orient="index")

    # Persist data in a local file
    utils.load_csv(result, C.NORMALIZED_VALUES.format(level))


def load_redshift(level:str="province") -> None:
    """Load the given dataset to the Redshift database"""

    c = Connection(db_name="redshift")
    c.load_env()
    redshift_con = c.create_con()

    level_table = C.GEO_LEVELS[level]["destination_table_suffix"]

    df_dataset = utils.get_data_csv(C.NORMALIZED_VALUES.format(level))
    
    df_dataset.to_sql(
        C.DESTINATION_TABLE.format(level_table), 
        con=redshift_con, 
        if_exists="replace", 
        index=False,
        method="multi"
        )
    