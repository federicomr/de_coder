import os
from dotenv import load_dotenv
import redshift_connector
from sqlalchemy import create_engine
from sqlalchemy.engine.url import URL
import pandas as pd

import sube.constants as C

class Connection:
    """
    Create and manage connections with a batabase
    
    Args:
        dialect(str): the dialect of the database to connect. Defaults to 
        redshift.
        db_name(str): name of the database to connect. It must match with the
        variables names in the env file.
    """
    def __init__(self, dialect='redshift+redshift_connector', db_name=None):
        self.db = db_name
        self.dialect = dialect

    def load_env(self,file_path:str=C.ENV_FILE):
        try:
            load_dotenv(file_path)
        except Exception as e:
            print(e)
        return 'Variables successfully imported.'

    def create_con(self):
        """
        Creates a connection to the Redshift database
        """
        db = self.db.upper()
        
        url_db = URL.create(
            drivername = "redshift+psycopg2",
            host = os.environ.get(f"{db}_HOST"),
            database = os.environ.get(f"{db}_DB"),
            port = os.environ.get(f"{db}_PORT"),
            username = os.environ.get(f"{db}_USER"),
            password = os.environ.get(f"{db}_PASSWORD")
        )

        redshift_con = create_engine(url_db)

        try:
            redshift_con.connect()
        except Exception as e:
            return e
        return redshift_con
    

    def execute_select(self, query:str) -> pd.DataFrame:
        """Execute a select statement against the Redshift Database.

        Args:
            query (str): The SELECT query.

        Returns:
            pd.DataFrame: A table with the desired output.
        """
        self.load_env()

        with self.create_con().connect() as redshift_con:
            r = redshift_con.execute(query)
            df = pd.DataFrame(r)
        
        return df
    

        
