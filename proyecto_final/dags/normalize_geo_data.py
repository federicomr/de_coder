from datetime import datetime

from airflow.models.dag import DAG
from airflow.operators.python import PythonOperator

# Import project functions
from sube import etl_geo_data 

with DAG(
    "Normalize_geographic_data",

    default_args={
        "start_date":datetime(2024, 4, 5),
        "depends_on_past": False,
        "email": ["federico.m.rapp@gmail.com"],
        "email_on_failure": False,
        "email_on_retry": False,
        "retries": 1
    },
    description="Build the normalize tables from geographic references in SUBE tables.",
    schedule="0 0 1 * *",
    catchup=False,
    tags=["sube","geo_data","normalize"],
) as dag:

    t1 = PythonOperator(
        task_id="get_unique_provinces",
        python_callable=etl_geo_data.get_unique_values,
        op_kwargs={"level": "province"}
    )

    t2 = PythonOperator(
        task_id="normalize_provinces",
        python_callable=etl_geo_data.normalize_all_values,
        op_kwargs={"level": "province"}
    )

    t3 = PythonOperator(
        task_id="load_redshift_provinces",
        python_callable=etl_geo_data.load_redshift,
        op_kwargs={"level": "province"}
    )

    t4 = PythonOperator(
        task_id="get_unique_cities",
        python_callable=etl_geo_data.get_unique_values,
        op_kwargs={"level": "city"}
    )

    t5 = PythonOperator(
        task_id="normalize_cities",
        python_callable=etl_geo_data.normalize_all_values,
        op_kwargs={"level": "city"}
    )

    t6 = PythonOperator(
        task_id="load_redshift_cities",
        python_callable=etl_geo_data.load_redshift,
        op_kwargs={"level": "city"}
    )
    

    t1 >> t2 >> t3
    t4 >> t5 >> t6