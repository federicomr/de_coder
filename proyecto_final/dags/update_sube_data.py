from datetime import datetime

from airflow.models.dag import DAG
from airflow.operators.python import PythonOperator

# Import project functions
from sube.sube_extract import download_year_file
from sube.sube_transform import transform_transactions, transform_unique_cards
from sube.sube_load import upsert_current_year_redshift


with DAG(
    "Update_SUBE_data",

    default_args={
        "start_date":datetime(2024, 4, 5),
        "depends_on_past": False,
        "email": ["federico.m.rapp@gmail.com"],
        "email_on_failure": False,
        "email_on_retry": False,
        "retries": 1
    },
    description="Update the transactions and unique cards  SUBE's data to the database",
    schedule="0 20 * * *",
    catchup=False,
    tags=["sube","transactions","unique_cards"],
) as dag:

    t1 = PythonOperator(
        task_id="extract_transactions",
        python_callable=download_year_file,
        op_kwargs={"dataset_name": "transactions"}
    )

    t2 = PythonOperator(
        task_id="transform_transactions",
        python_callable=transform_transactions
    )

    t3 = PythonOperator(
        task_id="load_transactions",
        python_callable=upsert_current_year_redshift,
        op_kwargs={"dataset_name": "transactions"}
    )

    t4 = PythonOperator(
        task_id="extract_unique_cards",
        python_callable=download_year_file,
        op_kwargs={"dataset_name": "unique_cards"}
    )

    t5 = PythonOperator(
        task_id="transform_unique_cards",
        python_callable=transform_unique_cards
    )

    t6 = PythonOperator(
        task_id="load_unique_cards",
        python_callable=upsert_current_year_redshift,
        op_kwargs={"dataset_name": "unique_cards"}
    )

    t1 >> t2 >> t3
    t4 >> t5 >> t6