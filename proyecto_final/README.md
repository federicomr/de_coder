## Proyecto final
El proyecto permite ejecutar la actualización diaria de los datos de SUBE que son volcados a la instancia de Amazon Redshift. 

## Datos publicos SUBE
Para el trabajo se seleccionó como fuente los datos abiertos de la tarjeta SUBE que son publicados en el portal oficial del gobierno nacional (https://datos.transporte.gob.ar).\
Se trabajó con dos datasets. Primero la cantidad de viajes (transacciones/transactions) realizados por dia, por tipo de transporte y linea. Segundo, la cantidad de usuarios (tarjetas únicas/unique cards) que viajaron por dia, tipo de transporte y tipo de tarifa.\
Por otro lado, se trabajó con los datos del Servicio de Normalización de Direcciones y Unidades Territoriales de Argentina a partir de la API Georref lo cual nos permite normalizar las unidades geográficas.\
El proyecto importa los datos de viajes y usuarios de SUBE a un Data Warehouse y los actualiza de forma diaria. Luego, normaliza los nombres de unidades territoriales lo que posibilita posteriores cruces con otros datasets.

## Prerequisitos
- Tener instalado Docker
- Contar con una conexión a una base de datos Redshift.
- Si las tablas destino no existen en la base de datos se pueden crear con los scripts en `esquema_db.sql`.

## Instalación
El proceso se ejecuta enteramente dentro de contenedores de Docker. Para levantar Airflow con los DAG que ejecutan el proceso es necesario seguir los siguientes pasos.

Primero, Debe contar con un archivo `.env` con las credenciales de acceso a la base de datos Redshift y la configuración del servicio de mail.̣ Puede copiar el modelo y completar con los datos correspondientes.

    $ cp env-template plugins/sube/.env

Segundo, si se quiere activar el servicio de alertas por correo electrónico será necesario completar las variables `EMAIL_ADDRESS_FROM` y `EMAIL_ADDRESS_TO` (con la dirección de correo del emisor y del remitente respectivamente) en el archivo `plugins/sube/constants.py`. De lo contrario, las alertas no se enviarán.

Segundo, el proyecto se ejecuta mediante un conjunto de contenedores conectados mediante Docker Compose. Para iniciarlizarlos ejecute el siguiente comando

    $ docker compose up airflow-init

Luego, debe levantar los servicios

    $ docker compose up -d

Finalmente, estará en condiciones de acceder a Airflow mediante la interfaz web con la url `http://localhost:8080/`. El usuario y contraseña por defecto son `airflow` y `airflow `.

>**Importante**: La definición de los datasets y las url de descarga de los archivos disponibles se encuentran en `plugins/sube/datasets_parameters.json`. Si algo llegará a cambiar respecto a su ubicación o se si suma un nuevo archivo de un nuevo año (cuando comience 2025 por ejemplo) es necesario modificarlo allí.


## Ejecución
Dentro de la interfaz web de Airflow encontrará dos DAG cuyas especificaciones se detallan a continuación.

| DAG archivo | DAG nombre | Ejecucion | codigo origen | Detalle |
| -------- | -------- | -------- | -------- | -------- |
| update_sube_data.py | Update_SUBE_data | Una vez al dia a las 20hs. | sube_extract.py, sube_transform.py, sube_load.py | Actualiza los datos de usos (transacciones) y usuarios (tarjetas unicas) del año en curso en el Data Warehouse. | 
| normalize_geo_data.py | Normalize_geographic_data | El primer dia de cada mes a las 00hs. | etl_geo_data.py | Genera las tablas de claves para normalizar los valores de provincias (normalizador_provincias) y municipios (normalizador_municipios). |
