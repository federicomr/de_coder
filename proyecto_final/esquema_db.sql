create table sube_transacciones_dia (
	fecha date not null,
	nombre_empresa varchar not null,
	linea varchar not null,
	es_amba varchar,
	tipo_transporte varchar,
	jurisccion varchar,
	provincia varchar,
	municipio varchar,
	cantidad int,
	dato_preliminar varchar
) diststyle auto;


create table sube_usuarios_dia (
	fecha date not null,
	es_amba varchar not null,
	motivo_tarifa_social varchar not null,
	genero varchar not null,
	tipo_transporte varchar not null,
	cantidad_tarjetas int,
	dato_preliminar varchar,
	primary key(fecha, es_amba, motivo_tarifa_social, genero, tipo_transporte)
) diststyle auto;


create table sube_usuarios_dia_totales (
	fecha date not null,
	es_amba varchar not null,
	motivo_tarifa_social varchar not null,
	genero varchar not null,
	cantidad_tarjetas int,
	dato_preliminar varchar,
	primary key(fecha, es_amba, motivo_tarifa_social, genero)
) diststyle auto;


create table normalizador_municipios (
	current_value varchar,
	normalized_value varchar
);


create table normalizador_provincias (
	current_value varchar,
	normalized_value varchar
);