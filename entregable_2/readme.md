## Entregable 2

## Datos publicos SUBE
Para el trabajo se seleccionó como fuente los datos abiertos de la tarjeta SUBE que son publicados en el portal oficial del gobierno nacional (https://datos.transporte.gob.ar).\
Se trabajó con dos datasets. Primero la cantidad de viajes (transacciones/transactions) realizados por dia, por tipo de transporte y linea. Segundo, la cantidad de usuarios (tarjetas únicas/unique cards) que viajaron por dia, tipo de transporte y tipo de tarifa.

## Pre requisitos
- El proceso fue desarrollado y probado con python 3.8. Se sugiere ejecutarlo con la misma versión.
  
## Instalación
Para ejecutar el proceso en primer lugar se deben instalar las librerias utilizadas en el proyecto.

    $ pip install -r requirements.txt

Luego, crear la carpeta _data_ dentro de la carpeta del proyecto.

    $ mkdir data

O cambiar la constante `CARPETA_DATOS` dentro del `archivo extraer_datos_sube.py` al nombre de carpeta donde se guardaran los csv descargados.\
Debe contar con un archivo `.env` con las credenciales de acceso a la base de datos Redshift.̣ Puede copiar el modelo y completar con los datos correspondientes.\

    $ cp env-template .env

Por último, asegurarse de contar con el archivo `datasets_parameters.json` en la carpeta principal del proyecto.

## Ejecución
El proceso se ejecuta mediante los comandos establecidos en `etl_command.py`. Para ejecutar el proceso ETL completo por primera vez, esto es, descargar todos los csv, transformarlos y subirlos a la base de datos, se debe ejecutar lo siguiente:

    $ python etl_command insert_complete_dataset {dataset-name}

Donde _dataset-name_ debe ser el nombre de alguno de los dos dataset contemplados (transactions o unique_cards).\

### Actualizacion diaria
En cambio si lo que se desea es actualizar de forma diaria los nuevos datos o actualizar los datos de un año anterior se deberá ejecutar el comando

    $ python etl_command update_dataset_year {anio} {dataset-name}

Donde `anio` es el año respecto del cual se quiere actualizar los datos y `dataset-name` alguno de los dos dataset contemplados. Por ejemplo:

    $ python etl_command update_dataset_year 2024 unique_cards

Si por algun motivo no puede utilizar click o ejecutar los comandos puede agregar las siguientes lineas al final del archivo `etl.py`

```python
# Version proceso completo
if __name__ == "__main__":
    run_year_proccess("unique_cards", 2024)
```
Los parámetros de _run_year_proccess_ son a modo de ejemplo.

## Modelo base de datos
El archivo `esquema_db.sql` contiene las sentencias sql para crear las dos tablas donde se guardarán los datos de cada uno de los datasets.
