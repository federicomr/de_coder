import click
import etl 

@click.group()
def etl_commands():
    pass

@etl_commands.command(help="Run the ETL proccess for a given dataset and year")
@click.argument("dataset_name")
@click.argument("year")
def update_dataset_year(dataset_name:str, year:str):
    etl.run_year_proccess(dataset_name, year)


@etl_commands.command(help="""Run the ETL proccess for all the existing files
                       and years""")
@click.argument("dataset_name")
def insert_complete_dataset(dataset_name:str):
    etl.run_initial_proccess(dataset_name)


if __name__ == "__main__":
    etl_commands()