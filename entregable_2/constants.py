import os

URL_BASE = "https://archivos-datos.transporte.gob.ar/upload"

ENV_FILE = ".env"

CARPETA_DATOS = "data"
RUTA_DATOS_SUBE = os.path.join(CARPETA_DATOS)

ARCHIVO_PARAMETROS_SUBE = "datasets_parameters.json"
#RUTA_ARCHIVOS_PARAMETROS = os.path.join(CARPETA_DATOS, ARCHIVO_PARAMETROS_SUBE)