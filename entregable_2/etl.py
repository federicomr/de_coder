import pendulum

import extract
import transform
import load
import utils


def run_year_proccess(dataset_name:str, 
                      year:str=str(pendulum.today().year), 
                      include_extract=True) -> None:
    """Executes the ETL proccess for a single dataset and year. 

    Args:
        dataset_name (str): Name of the dataset to proccess.
        year (str, optional): Year of the data to proccess. 
        Defaults to str(pendulum.today().year).
        include_extract (bool, optional): If True runs the extract step 
        first, if False avoid the extract and only runs the TL steps. 
        Defaults to True.
    """
        
    dataset_parameters = utils.get_datasets_parameters()[dataset_name]
    
    # Extract
    if include_extract == True:
        extract.download_year_file(dataset_name, dataset_parameters, year)

    file_name = dataset_parameters["csv_files"][year]
    
     # Get the year dataset file
    dataset_year = utils.get_data_csv(f"{dataset_name}_{file_name}")
    
    if dataset_name == "unique_cards":
        # Transform dataset
        transform.transform_unique_cards(dataset_year)
        unique_cards = utils.get_data_csv("unique_cards.csv")
        unique_cards_daily_totals = (
            utils.get_data_csv("unique_cards_daily_total.csv")
        )
        
        # Load data to database
        ## First delete all rows of the current year and the load the new data.
        load.delete_records_redshift(
            dataset_parameters["destination_table"]["unique_cards"], year
            )
        load.load_redshift(
            unique_cards, 
            dataset_parameters["destination_table"]["unique_cards"]
            )
        
        load.delete_records_redshift(
            dataset_parameters["destination_table"]["unique_cards_daily_totals"],
            year
            )
        load.load_redshift(
            unique_cards_daily_totals, 
            dataset_parameters["destination_table"]["unique_cards_daily_totals"]
            )
    
    elif dataset_name == "transactions":
        # Transform dataset
        transform.transform_transactions(dataset_year)
        transactions = utils.get_data_csv("transactions.csv")

        # Load data to database
        load.load_redshift(
            transactions, 
            dataset_parameters["destination_table"]
            )
        

def run_initial_proccess(dataset_name:str) -> None:
    """Run the proccess for the first load of dataset historic data.

    Args:
        dataset_name (str): Name of the dataset to load.
    """
    dataset_parameters = utils.get_datasets_parameters()[dataset_name]

    # Extract
    extract.download_historic_data(dataset_name, dataset_parameters)

    # Transform and load
    for year, _ in dataset_parameters["csv_files"].items():
        run_year_proccess(dataset_name, year, False)