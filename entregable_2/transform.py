import pandas as pd
import pendulum

import utils
import constants as C


def consolidate_dataset(dataset_name:str, 
                        destination_folder:str=C.RUTA_DATOS_SUBE) -> None:
    """Concatenate all csv files of a dataset in one table and export it to a 
    new consolidated dataset csv file.

    Args:
        file_name (str): Name of the dataset to consolidate
        source_folder (str): Route of the folder with the csv files.

    Returns:
        None: Export a csv file.
    """ 
    parameters = utils.get_datasets_parameters()
    dataset_parameters = parameters[dataset_name]

    consolidated_dataset = pd.DataFrame()

    for _, csv_file_name in dataset_parameters["csv_files"].items():
        df_year = utils.get_data_csv(f"{dataset_name}_{csv_file_name}")
        consolidated_dataset = pd.concat([consolidated_dataset, df_year])

    utils.load_csv(consolidated_dataset, 
                   f"{dataset_name}_complete.csv", 
                   destination_folder)


def transform_unique_cards(unique_cards:pd.DataFrame) -> None:
    """Transform the unique cards dataset, separate the daily totals in a new
    DataFrame and left them ready to load to the database. Finally, saves both
    DataFrames in csv files in a local folder. 

    Args:
        df_unique_cards (pd.DataFrame): The unique cards table to transform.
    """
    transformed_cards = unique_cards.copy()

    # Rename columns to database normalized names
    parameters = utils.get_datasets_parameters()
    cards_parameters = parameters["unique_cards"]

    transformed_cards = transformed_cards.rename(
        columns=cards_parameters["columns"]
        )
    
    # Fill NA values in primary key fields
    transformed_cards["motivo_tarifa_social"] = (
        transformed_cards["motivo_tarifa_social"].fillna("SIN TARIFA SOCIAL")
    )
    transformed_cards["genero"] = (
        transformed_cards["genero"].fillna("SIN DATOS")
    )
    
    
    # Extract the daily total from the unique cards table.
    unique_cards_daily_total = (
        transformed_cards.loc[transformed_cards["tipo_transporte"] == "TOTAL"]
        .drop(columns=["tipo_transporte"])
        .copy()
    )

    transformed_cards.drop(unique_cards_daily_total.index, inplace=True)

    # Assign the right data type to date field.
    transformed_cards["fecha"] = (
        pd.to_datetime(transformed_cards["fecha"])
    )

    # Save transformed datasets in the local folder.
    utils.load_csv(transformed_cards, "unique_cards.csv")
    utils.load_csv(unique_cards_daily_total, "unique_cards_daily_total.csv")


def transform_transactions(transactions:pd.DataFrame) -> None:
    """Transform the transactions dataset. Left it ready to load to the 
    database. Finally, saves the DataFrame in csv files in a local folder. 

    Args:
        transactions (pd.DataFrame): The transactions table to transform.
    """
    transformed_transactions = transactions.copy()

    # Rename columns to database normalized names
    parameters = utils.get_datasets_parameters()
    transactions_parameters = parameters["transactions"]

    transformed_transactions = transformed_transactions.rename(
        columns=transactions_parameters["columns"]
        )
    
    # Change date column data type
    transformed_transactions["fecha"] = (
        pd.to_datetime(transformed_transactions["fecha"])
        )

    # Replace null values in province and department columns
    transformed_transactions["provincia"] = (
        transformed_transactions["provincia"].replace(
            "JN", 
            "JURISDICCION NACIONAL", 
            regex=False)
    )
    transformed_transactions["municipio"] = (
        transformed_transactions["municipio"].replace(
            "SN|SD", 
            pd.NA, 
            regex=True)
    )

    # Save it to a local file
    utils.load_csv(transformed_transactions, "transactions.csv")


def run_transform(dataset_name:str, mode:str="current_year") -> None:
    """Executes the transformation.

    Args:
        dataset_name (str): Name of the dataset to transform
        mode (str): If the transformation process should concatenate multiple
        dataset files (complete) or only run for the current year dataset 
        file.
    """
    transform_functions = {
        "transactions": transform_transactions,
        "unique_cards": transform_unique_cards
    }

    dataset_parameters = utils.get_datasets_parameters()[dataset_name]

    # Get the dataset. If mode is complete first consolidate all dataset files
    # in one DataFrame.
    if mode == "complete":
        dataset = consolidate_dataset(dataset_name)

    elif mode == "current_year":
        current_year = str(pendulum.today().year)

        file_name = dataset_parameters["archivos_csv"][current_year]
        dataset = utils.get_data_csv(f"{dataset_name}_{file_name}")
    
    else:
        raise ValueError(f"Invalid mode value: {mode}")
    
    # Apply transformations
    transform_functions[dataset_name](dataset)    
    